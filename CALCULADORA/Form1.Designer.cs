﻿namespace CALCULADORA
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBorrar = new System.Windows.Forms.Button();
            this.btnSigno = new System.Windows.Forms.Button();
            this.btnDivision = new System.Windows.Forms.Button();
            this.num7 = new System.Windows.Forms.Button();
            this.num8 = new System.Windows.Forms.Button();
            this.num9 = new System.Windows.Forms.Button();
            this.btnMultiplicacion = new System.Windows.Forms.Button();
            this.num4 = new System.Windows.Forms.Button();
            this.num5 = new System.Windows.Forms.Button();
            this.num6 = new System.Windows.Forms.Button();
            this.bntMenos = new System.Windows.Forms.Button();
            this.num1 = new System.Windows.Forms.Button();
            this.num2 = new System.Windows.Forms.Button();
            this.num3 = new System.Windows.Forms.Button();
            this.btnMas = new System.Windows.Forms.Button();
            this.num0 = new System.Windows.Forms.Button();
            this.btnIgual = new System.Windows.Forms.Button();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(42, 79);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(107, 38);
            this.btnBorrar.TabIndex = 2;
            this.btnBorrar.Text = "AC";
            this.btnBorrar.UseVisualStyleBackColor = true;
            // 
            // btnSigno
            // 
            this.btnSigno.Location = new System.Drawing.Point(171, 79);
            this.btnSigno.Name = "btnSigno";
            this.btnSigno.Size = new System.Drawing.Size(48, 38);
            this.btnSigno.TabIndex = 3;
            this.btnSigno.Text = "+/-";
            this.btnSigno.UseVisualStyleBackColor = true;
            // 
            // btnDivision
            // 
            this.btnDivision.Location = new System.Drawing.Point(242, 79);
            this.btnDivision.Name = "btnDivision";
            this.btnDivision.Size = new System.Drawing.Size(48, 38);
            this.btnDivision.TabIndex = 4;
            this.btnDivision.Text = "/";
            this.btnDivision.UseVisualStyleBackColor = true;
            // 
            // num7
            // 
            this.num7.Location = new System.Drawing.Point(42, 141);
            this.num7.Name = "num7";
            this.num7.Size = new System.Drawing.Size(48, 38);
            this.num7.TabIndex = 5;
            this.num7.Text = "7";
            this.num7.UseVisualStyleBackColor = true;
            this.num7.Click += new System.EventHandler(this.num7_Click);
            // 
            // num8
            // 
            this.num8.Location = new System.Drawing.Point(101, 141);
            this.num8.Name = "num8";
            this.num8.Size = new System.Drawing.Size(48, 38);
            this.num8.TabIndex = 6;
            this.num8.Text = "8";
            this.num8.UseVisualStyleBackColor = true;
            this.num8.Click += new System.EventHandler(this.num8_Click);
            // 
            // num9
            // 
            this.num9.Location = new System.Drawing.Point(171, 141);
            this.num9.Name = "num9";
            this.num9.Size = new System.Drawing.Size(48, 38);
            this.num9.TabIndex = 7;
            this.num9.Text = "9";
            this.num9.UseVisualStyleBackColor = true;
            this.num9.Click += new System.EventHandler(this.num9_Click);
            // 
            // btnMultiplicacion
            // 
            this.btnMultiplicacion.Location = new System.Drawing.Point(242, 141);
            this.btnMultiplicacion.Name = "btnMultiplicacion";
            this.btnMultiplicacion.Size = new System.Drawing.Size(48, 38);
            this.btnMultiplicacion.TabIndex = 8;
            this.btnMultiplicacion.Text = "x";
            this.btnMultiplicacion.UseVisualStyleBackColor = true;
            // 
            // num4
            // 
            this.num4.Location = new System.Drawing.Point(42, 198);
            this.num4.Name = "num4";
            this.num4.Size = new System.Drawing.Size(48, 38);
            this.num4.TabIndex = 9;
            this.num4.Text = "4";
            this.num4.UseVisualStyleBackColor = true;
            this.num4.Click += new System.EventHandler(this.num4_Click);
            // 
            // num5
            // 
            this.num5.Location = new System.Drawing.Point(101, 198);
            this.num5.Name = "num5";
            this.num5.Size = new System.Drawing.Size(48, 38);
            this.num5.TabIndex = 10;
            this.num5.Text = "5";
            this.num5.UseVisualStyleBackColor = true;
            this.num5.Click += new System.EventHandler(this.num5_Click);
            // 
            // num6
            // 
            this.num6.Location = new System.Drawing.Point(171, 198);
            this.num6.Name = "num6";
            this.num6.Size = new System.Drawing.Size(48, 38);
            this.num6.TabIndex = 11;
            this.num6.Text = "6";
            this.num6.UseVisualStyleBackColor = true;
            this.num6.Click += new System.EventHandler(this.num6_Click);
            // 
            // bntMenos
            // 
            this.bntMenos.Location = new System.Drawing.Point(242, 198);
            this.bntMenos.Name = "bntMenos";
            this.bntMenos.Size = new System.Drawing.Size(48, 38);
            this.bntMenos.TabIndex = 12;
            this.bntMenos.Text = "-";
            this.bntMenos.UseVisualStyleBackColor = true;
            // 
            // num1
            // 
            this.num1.Location = new System.Drawing.Point(42, 251);
            this.num1.Name = "num1";
            this.num1.Size = new System.Drawing.Size(48, 38);
            this.num1.TabIndex = 13;
            this.num1.Text = "1";
            this.num1.UseVisualStyleBackColor = true;
            this.num1.Click += new System.EventHandler(this.num1_Click);
            // 
            // num2
            // 
            this.num2.Location = new System.Drawing.Point(101, 251);
            this.num2.Name = "num2";
            this.num2.Size = new System.Drawing.Size(48, 38);
            this.num2.TabIndex = 14;
            this.num2.Text = "2";
            this.num2.UseVisualStyleBackColor = true;
            this.num2.Click += new System.EventHandler(this.num2_Click);
            // 
            // num3
            // 
            this.num3.Location = new System.Drawing.Point(171, 251);
            this.num3.Name = "num3";
            this.num3.Size = new System.Drawing.Size(48, 38);
            this.num3.TabIndex = 15;
            this.num3.Text = "3";
            this.num3.UseVisualStyleBackColor = true;
            this.num3.Click += new System.EventHandler(this.num3_Click);
            // 
            // btnMas
            // 
            this.btnMas.Location = new System.Drawing.Point(242, 251);
            this.btnMas.Name = "btnMas";
            this.btnMas.Size = new System.Drawing.Size(48, 38);
            this.btnMas.TabIndex = 16;
            this.btnMas.Text = "+";
            this.btnMas.UseVisualStyleBackColor = true;
            // 
            // num0
            // 
            this.num0.Location = new System.Drawing.Point(42, 306);
            this.num0.Name = "num0";
            this.num0.Size = new System.Drawing.Size(177, 37);
            this.num0.TabIndex = 17;
            this.num0.Text = "0";
            this.num0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.num0.UseVisualStyleBackColor = true;
            this.num0.Click += new System.EventHandler(this.button16_Click);
            // 
            // btnIgual
            // 
            this.btnIgual.Location = new System.Drawing.Point(242, 306);
            this.btnIgual.Name = "btnIgual";
            this.btnIgual.Size = new System.Drawing.Size(48, 37);
            this.btnIgual.TabIndex = 18;
            this.btnIgual.Text = "=";
            this.btnIgual.UseVisualStyleBackColor = true;
            // 
            // txtResultado
            // 
            this.txtResultado.Location = new System.Drawing.Point(42, 29);
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.Size = new System.Drawing.Size(248, 20);
            this.txtResultado.TabIndex = 19;
            this.txtResultado.Text = "0";
            this.txtResultado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 394);
            this.Controls.Add(this.txtResultado);
            this.Controls.Add(this.btnIgual);
            this.Controls.Add(this.num0);
            this.Controls.Add(this.btnMas);
            this.Controls.Add(this.num3);
            this.Controls.Add(this.num2);
            this.Controls.Add(this.num1);
            this.Controls.Add(this.bntMenos);
            this.Controls.Add(this.num6);
            this.Controls.Add(this.num5);
            this.Controls.Add(this.num4);
            this.Controls.Add(this.btnMultiplicacion);
            this.Controls.Add(this.num9);
            this.Controls.Add(this.num8);
            this.Controls.Add(this.num7);
            this.Controls.Add(this.btnDivision);
            this.Controls.Add(this.btnSigno);
            this.Controls.Add(this.btnBorrar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.Button btnSigno;
        private System.Windows.Forms.Button btnDivision;
        private System.Windows.Forms.Button num7;
        private System.Windows.Forms.Button num8;
        private System.Windows.Forms.Button num9;
        private System.Windows.Forms.Button btnMultiplicacion;
        private System.Windows.Forms.Button num4;
        private System.Windows.Forms.Button num5;
        private System.Windows.Forms.Button num6;
        private System.Windows.Forms.Button bntMenos;
        private System.Windows.Forms.Button num1;
        private System.Windows.Forms.Button num2;
        private System.Windows.Forms.Button num3;
        private System.Windows.Forms.Button btnMas;
        private System.Windows.Forms.Button num0;
        private System.Windows.Forms.Button btnIgual;
        private System.Windows.Forms.TextBox txtResultado;
    }
}

